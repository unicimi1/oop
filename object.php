<?php

require 'class.php';


$person1 = new Person();
$person1->name = 'Agus';
$person1->address = 'Jogjakarta';

$person2 = new Person();
$person2->name = 'Budi';
$person2->address = 'Johor';
$person2->country = 'Malaysia';

echo "Nama : $person1->name<br>";
echo "Alamat : $person1->address<br>";
echo "Negara : $person1->country<br>";

echo "Nama : $person2->name<br>";
echo "Alamat : $person2->address<br>";
echo "Negara : $person2->country<br>";
